class MembroTarefa {
    
    constructor(tarefa, membro) {
        
        this._tarefa = tarefa;
        this._membro = membro;
        Object.freeze(this);
    }
    
    get tarefa() {
        
        return this._tarefa;
    }

    get membro() {
        
        return this._membro;
    }
    

}