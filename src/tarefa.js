class Tarefa {
    
    constructor(id, titulo, descricao, dataIni, dataFim, ckStatus, ckNome,status, dataRini, dataRfim, timestamp, gerente) {
        
        this._id = id;
        this._titulo = titulo;
        this._descricao = descricao;
        this._dataIni = dataIni;
        this._dataFim = dataFim;
        this._ckStatus = ckStatus;
        this._ckNome = ckNome;
        this._status = status;
        this._dataRini = dataRini;
        this._dataRfim = dataRfim;
        this._timestamp = new Date(timestamp.getTime());
        this._gerente = gerente;
    }
    
    get id() {
        
        return this._id;
    }

    get titulo() {
        
        return this._titulo;
    }
    
    get descricao() {
        
        return this._descricao;
    }
    get dataIni() {
        
        return this._dataIni;
    }

    get dataFim() {
        
        return this._dataFim;
    }
    
    get ckStatus() {
        
        return this._ckStatus;
    }
    get ckNome() {
        
        return this._ckNome;
    }

    get status() {
        
        return this._status;
    }
    
    get dataRini() {
        
        return this._dataRini;
    }
    get dataRfim() {
        
        return this._dataRfim;
    }

    get gerente() {
        
        return this._gerente;
    }
    
    get timestamp() {
        
        return new Date(this._timestamp.getTime());
    }
    
    listaTarefas(){
        const Dao = require('./dao');

        app.get('/tarefa', (req, res, next) => 
            new dao(req.connection)
            .list()
            .then(tb_tarefa => res.json(tb_tarefa))
            .catch(next)
        );
    }
    
}