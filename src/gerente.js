class Gerente{
    
    constructor(id, cpf, nome) {
        
        this._id = id;
        this._cpf = cpf;
        this._nome = nome;
        Object.freeze(this);
    }
    
    get id() {
        
        return this._id;
    }

    get cpf() {
        
        return this._cpf;
    }
    
    get nome() {
        
        return this._nome;
    }

}