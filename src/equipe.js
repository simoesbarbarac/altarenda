class Equipe{
    
    constructor(id, gerente, data) {
        
        this._id = id;
        this._gerente = gerente;
        this._data = data;
        Object.freeze(this);
    }
    
    get id() {
        
        return this._id;
    }

    get gerente() {
        
        return this._gerente;
    }
    
    get data() {
        
        return this._data;
    }

}