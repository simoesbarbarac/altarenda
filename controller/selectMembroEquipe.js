$(document).ready(function(){
                                const url2 = 'http://localhost:3000/Ambrosia/src/selectMembroEquipe.js';
                                var dadosArquivados = JSON.parse(sessionStorage.getItem('chave'));
                                console.log('dados arquivados'+dadosArquivados);
                                  vData = {dadosArquivados:dadosArquivados}
                                  $.ajax({
                                    url:url2,
                                    type:"POST",
                                    data:vData,
                                    success: function(result){
                                      var response = result;
                                      var arraydearray = [];  

                                      $.each(response, function () {
                                          var array = [];
                                          $.each(this, function (k, v) {

                                            if (k == 'mem_cpf' && v!= null){
                                              array.push(v)
                                            }
                                            if (k == 'mem_nome' && v!= null){
                                              array.push(v)
                                            }
                                            
                                          }) 
                                          arraydearray.push(array)               
                                      })
                                      $(document).ready(function() {
                                        var nColNumber = -1; 
                                        var table = $('#table').DataTable( {
                                            data: arraydearray,
                                            columns: [
                                                { 'targets': [ ++nColNumber ],title: "CPF" },
                                                { 'targets': [ ++nColNumber ],title: "Nome" }
                                            ],
                                            "language": {
                                              "sEmptyTable": "Nenhum registro encontrado",
                                              "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                              "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                              "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                              "sInfoPostFix": "",
                                              "sInfoThousands": ".",
                                              "sLengthMenu": "_MENU_ resultados por página",
                                              "sLoadingRecords": "Carregando...",
                                              "sProcessing": "Processando...",
                                              "sZeroRecords": "Nenhum registro encontrado",
                                              "sSearch": "Pesquisar",
                                              "oPaginate": {
                                                  "sNext": "Próximo",
                                                  "sPrevious": "Anterior",
                                                  "sFirst": "Primeiro",
                                                  "sLast": "Último"
                                              },
                                              "oAria": {
                                                  "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                  "sSortDescending": ": Ordenar colunas de forma descendente"
                                              },
                                              "select": {
                                                  "rows": {
                                                      "_": "Selecionado %d linhas",
                                                      "0": "Nenhuma linha selecionada",
                                                      "1": "Selecionado 1 linha"
                                                  }
                                                }}
                                        } );
                                        
                                        $('#table').on('click', 'tr', function () {
                                            var data = table.row( this ).data();
                                            var dadosMembro = JSON.stringify(data[1]);
                                            sessionStorage.setItem('chave', dadosMembro );
                                            console.log('enviando dados' + dadosMembro)  
                                            window.location.href ='/Users/BarbaraSimoes/Desktop/Ambrosia/ambrosia/views/alterarMembro.html';
                                        } );
                                    } );
                                      console.log('success')
                                    },
                                    error: function(error){
                                      console.log(error.status)  
                                      alert("Erro ao consultar equipe!");
                                      
                                    }
                                  })
                                })

