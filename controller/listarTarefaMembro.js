 $(document).ready(function(){
                                const url = 'http://localhost:3000/Ambrosia/src/listarTarefasMembro.js';
                                var dadosArquivados = JSON.parse(sessionStorage.getItem('usuario'));
                                console.log('dados arquivados'+dadosArquivados);
                                Vdata = {membro:dadosArquivados}
                    
                                  $.ajax({
                                    url:url,
                                    type:"POST",
                                    data:Vdata,
                                    success: function(result){
                                        var odd_even = false;
                                        var response = result;
                                        var arraydearray = [];

                                        $.each(response, function () {
                                          var array = [];
                                            $.each(this, function (k, v) {
                                              //console.log(v)
                                              if (k == 'tare_id' && v!= null){
                                                array.push(v)
                                              }
                                              if (k == 'tare_titulo' && v!= null){
                                                array.push(v) 
                                              }
                                              if (k == 'tare_descri' && v!= null){
                                                array.push(v) 
                                              }
                                              if (k == 'tare_status' && v!= null){
                                                array.push(v)
                                              }
                                            }) 
                                            arraydearray.push(array) 
                                        })
                                        $(document).ready(function() {
                                          var nColNumber = -1; 
                                          var table = $('#table').DataTable( {
                                              data: arraydearray,
                                              columns: [
                                                  { 'targets': [ ++nColNumber ],title: "Número" },
                                                  { 'targets': [ ++nColNumber ],title: "Descrição" },
                                                  { 'targets': [ ++nColNumber ],title: "Título" },
                                                  { 'targets': [ ++nColNumber ],title: "Status" }
                                              ],
                                              "language": {
                                                "sEmptyTable": "Nenhum registro encontrado",
                                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                                                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                                                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                                                "sInfoPostFix": "",
                                                "sInfoThousands": ".",
                                                "sLengthMenu": "_MENU_ resultados por página",
                                                "sLoadingRecords": "Carregando...",
                                                "sProcessing": "Processando...",
                                                "sZeroRecords": "Nenhum registro encontrado",
                                                "sSearch": "Pesquisar",
                                                "oPaginate": {
                                                    "sNext": "Próximo",
                                                    "sPrevious": "Anterior",
                                                    "sFirst": "Primeiro",
                                                    "sLast": "Último"
                                                },
                                                "oAria": {
                                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                                },
                                                "select": {
                                                    "rows": {
                                                        "_": "Selecionado %d linhas",
                                                        "0": "Nenhuma linha selecionada",
                                                        "1": "Selecionado 1 linha"
                                                    }
                                                  }}
                                          } );
                                          
                                        
                                          
                                          $('#table').on('click', 'tr', function () {
                                              var data = table.row( this ).data();
                                              var dadosTarefa = JSON.stringify(data[0]);
                                              sessionStorage.setItem('chave', dadosTarefa);
                                              console.log('enviando dados' + dadosTarefa)  
                                              window.location.href ='/Users/BarbaraSimoes/Desktop/Ambrosia/ambrosia/views/alterarTarefaMembro.html';
                                              //alert( 'You clicked on '+data[0]+' row' );
                                          } );
                                      } );
                                      console.log('success')
                                    },
                                    error: function(error){
                                      console.log(error.status)  
                                        alert("Erro ao buscar as tarefas!");
                                      
                                      
                                      
                                    }
                                  })
                                }) 