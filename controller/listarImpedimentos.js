function formatadata(data){
  var ano = data.substring(0,4);
  var mes = data.substring(5,7);
  var dia = data.substring(8,10);
  data = dia + '/' + mes + '/' + ano;
  return data;
 } 

$(document).ready(function(){
                              const url = 'http://localhost:3000/listarImpedimento';
                              var usuario = JSON.parse(sessionStorage.getItem('usuario'));
                              vData = { usuario:usuario}
                                $.ajax({
                                  url:url,
                                  type:"POST",
                                  data:vData,
                                  success: function(result){
                                      var odd_even = false;
                                      var response = result;
                                      console.log(result)

                                      if (result.length != 0){

                                      var head = "<thead class='thead-inverse'><tr>";
                                      $.each(response[0], function (k, v) {
                                        if (k == 'imp_id'){
                                          head = head + "<th scope='row'>" + "Número do Impedimento"+ "</th>";
                                        }
                                        if (k == 'imp_msg'){
                                          head = head + "<th scope='row'>" + "Descrição do Impedimento"+ "</th>";
                                        }
                                        if (k == 'imp_data'){
                                          head = head + "<th scope='row'>" + "Data de reporte"+ "</th>";
                                        }

                                        if (k == 'tare_titulo'){
                                          head = head + "<th scope='row'>" + "Título da tarefa"+ "</th>";
                                        }

                                        if (k == 'imp_status'){
                                          head = head + "<th scope='row'>" + "Status do Impedimento"+ "</th>";
                                        }
                                      })
                                      head = head + "</thead></tr>";
                                      $('#tableImpedimento').append(head);//append header
                                  var body="<tbody><tr>";
                                      $.each(response, function () {
                                          body=body+"<tr> class='table-danger'";
                                          $.each(this, function (k, v) {
                                            //console.log(v)
                                            if (k == 'imp_id' && v!= null){
                                              body=body +"<td>"+v.toString()+"</td>"; 
                                            }
                                            if (k == 'imp_msg' && v!= null){
                                              body=body +"<td>"+v.toString()+"</td>"; 
                                            }
                                            if (k == 'imp_data' && v!= null){
                                              body=body +"<td>"+formatadata(v.toString())+"</td>"; 
                                            }
                                            if (k == 'tare_titulo'&& v!= null){
                                              body=body +"<td>"+v.toString()+"</td>"; 
                                            }
                                            if (k == 'imp_status'&& v!= null){
                                              body=body +"<td>"+v.toString()+"</td>"; 
                                            }
                                          }) 
                                          body=body+"</tr>";               
                                      })
                                      body=body +"</tbody>";
                                      $(tableImpedimento).append(body);
                                    console.log('success')

                                    $('#tableImpedimento').on('click', 'tr', function () {
                                      var data = $(this).closest("tr").text()
                                      console.log(data)
                                      var dadosTarefa = JSON.stringify(data[0]);
                                      sessionStorage.setItem('chave', dadosTarefa);
                                      console.log('enviando dados' + dadosTarefa)  
                                      window.location.href ='/Users/BarbaraSimoes/Desktop/Ambrosia/ambrosia/views/alterarImpedimento.html';
                                      //alert( 'You clicked on '+data[0]+' row' );
                                  } );
                                  }else{
                                      document.getElementById("demo").innerHTML = 'Não há novos impedimentos'
                                  }
                              
                              },
                                  error: function(error){
                                    console.log(error.status)  
                                      alert("Erro ao buscar impedimentos!");
                                    
                                    
                                    
                                  }
                                })
                              }) 

