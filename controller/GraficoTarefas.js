$(document).ready(function () {
          const url = 'http://127.0.0.1:3000/Ambrosia/src/graficoTarefa.js';
          var usuario = JSON.parse(sessionStorage.getItem('usuario'));
          vData = { usuario:usuario}
          $.ajax({
            url:url,
            type:"POST",
             data:vData,
            success: function (result) {
              var response = result;
              var status = [];
              var quantidade = [];
              var key = 0;
              console.log(result)

              $.each(response, function () {
                $.each(this, function (k, v) {
                    if (k == 'tare_status') {
                        status.push(v);
                      }
                    if ((k == 'count(tare_id)') && (v != null) ) {
                    quantidade.push(v);
                    }
                    key++;
                })

              })
              var ctx = document.getElementById('myChart').getContext('2d');
              console.log(quantidade);
              console.log(status);
              
              var myChart = new Chart(ctx, {
                type: "bar",
                data:{
                labels: status,
                datasets: [{
                  label: 'Quantidade de Tarefas',
                  fillColor: '#404040',
                  data: quantidade,
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
                }
                ],
                options: {
                  scales: {
                    yAxes: [{
                      ticks: {
                        beginAtZero: true
                      }
                    }]
                  }
                }
            }
              });
              console.log('success')
            },
            error: function (error) {
              console.log(error.status)
              if (error.status != 0) {
                alert("Erro ao gerar gráfico!");
              }


            }
          })
        })