function formatadata(data){
  var ano = data.substring(0,4);
  var mes = data.substring(5,7);
  var dia = data.substring(8,10);
  data = dia + '/' + mes + '/' + ano;
  return data;
 }  
 
 
 $(document).ready(function(){
  var dadosArquivados = JSON.parse(sessionStorage.getItem('chave'));
  console.log('dados arquivados'+dadosArquivados);
                                const url = 'http://localhost:3000/Ambrosia/src/select.js';
                                vData = {dadosArquivados:dadosArquivados}
                                  $.ajax({
                                    url:url,
                                    type:"POST",
                                    data:vData,
                                    success: function(result){

                                      var numero = $("#numero");
                                      var titulo = $("#titulo");
                                      var descricao = $("#descricao"); 
                                      var dataIni = $("#calendario"); 
                                      var dataFim = $("#calendario1"); 
                                      var dataRIni = $("#calendario2"); 
                                      var dataRFim = $("#calendario3"); 
                                      var status = $("#status");

                                      status.val(result.tare_status);
                                      numero.val(dadosArquivados);
                                      titulo.val(result.tare_titulo);
                                      descricao.val(result.tare_descri);
                                      dataIni.val(formatadata(result.tare_dtIni.toString()));
                                      dataFim.val(formatadata(result.tare_dtEnc.toString()));
                                      if (result.tare_dtRealIni != null )
                                        dataRIni.val(formatadata(result.tare_dtRealIni.toString()));
                                      if (result.tare_dtRealEnc != null)  
                                        dataRFim.val(formatadata(result.tare_dtRealEnc.toString()));
                                      
                                      
                                      console.log('success')  
                                    },
                                    error: function(error){
                                      console.log(error.status)  
                                      alert("Erro ao consultar a tarefa!");
                                      
                                    }
                                  })
                                  
                                })
