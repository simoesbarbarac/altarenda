
const http = require('http');

const server = http.createServer();

server.on('request', function (request, response) {
  response.write('Hello ');
  response.end();
});

server.listen(3000);
console.log('Server is running.');