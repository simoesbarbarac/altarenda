var express = require('express');
var engines = require('consolidate');
var bodyParser = require('body-parser');
var banco = require('./src/create-table.js')
var app     = express();
var moment = require('moment');
const cors = require('cors');
const flash = require('express-flash-notification');
const cookieParser = require('cookie-parser');
const session = require('express-session');
var bcrypt = require ('bcrypt');



app.use(cors())
app.engine('html', engines.mustache);
app.set('view engine', 'html');
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(cookieParser());
app.use(session({secret: 'ssshhhhh', 
resave: true,
saveUninitialized: true}));
app.use(flash(app));
app.set('ambrosia', __dirname);
app.use(express.static(__dirname + '/views'));
var salt = bcrypt.genSaltSync(10);

app.post('/auth', function(request, response) {

	var username = request.body.cpf;
  var password = request.body.senha;
  console.log(password)
  var sql = "SELECT mem_senha, mem_id FROM tb_membro WHERE mem_cpf = '"+username+"'"
	if (username && password) {
    
		banco.connection.query(sql, function(err, results) {
      if (err) throw err;

			if (results.length > 0) {
        console.log(bcrypt.compareSync(password, results[0].mem_senha))
        if (bcrypt.compareSync(password, results[0].mem_senha)) {
        console.log("Membro encontrado");
        console.log(results[0]);
        response.header('Access-Control-Allow-Origin', request.headers.origin || "*");
        response.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
        response.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
        response.send(results[0]);
        }
        
				
			} else {
        var sql = "SELECT gere_id, gere_senha FROM tb_gerente WHERE gere_cpf = '"+username+"'"
        banco.connection.query(sql, function(err, result) {
          if (err) throw err;
          
          if (result.length > 0) {
            if (bcrypt.compareSync(password, result[0].gere_senha)) {
              console.log("gerente encontrado");
              console.log(result);
              request.session.loggedin = true;
              request.session.userId = result.gere_id;
              request.session.username = username;
              response.header('Access-Control-Allow-Origin', request.headers.origin || "*");
              response.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
              response.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
              response.send(result[0]);
            }
          }else{
            response.send('Incorrect Username and/or Password!');
            response.end();
          }
          
        });
        }	
     
		});
	} else {
		response.send('Please enter Username and Password!');
		response.end();
	}
});



app.post('/Ambrosia/src/select.js', function(req, res) {
  var row;

  var sql = "SELECT a.tare_titulo, a.tare_descri, a.tare_dtIni, a.tare_dtEnc, a.tare_dtRealIni,"+ 
            "a.tare_dtRealEnc, a.tare_status FROM tb_tarefa a "+
            "WHERE  a.tare_id = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.send(row);
  });
     
});

app.post('/Ambrosia/src/selectMembrosTarefa.js', function(req, res) {
  var row = [];

  var sql = "SELECT  b.mem_id, a.mem_nome FROM tb_membro_tarefa b  join tb_membro a on a.mem_id = b.mem_id "+
            " WHERE  b.tare_id = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      console.log(result[key])
        row[key] = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.send(row);
  });
     
});

app.post('/Ambrosia/src/selectMembro.js', function(req, res) {
  var row;
  console.log(req.body.dadosArquivados)
  var sql = "SELECT * FROM tb_membro WHERE  mem_nome = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      console.log(row)
      res.send(row);
  });
     
});

app.post('/Ambrosia/src/selectGerente.js', function(req, res) {
  var row;

  var sql = "SELECT * FROM tb_gerente WHERE  gere_nome = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      console.log(row)
      res.send(row);
  });
     
});

app.post('/Ambrosia/src/selectEquipe.js', function(req, res) {
  var row;

  var sql = "SELECT * FROM tb_equipe WHERE  equi_id = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.send(row);
  });
     
});

app.post('/selectImpedimento', function(req, res) {
  var row;

  var sql = "SELECT a.imp_id, a.imp_status, a.imp_msg, a.imp_data, b.tare_titulo  FROM tb_impedimento a join tb_tarefa b on a.imp_tarefa = b.tare_id WHERE  imp_id = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row = result[key];
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.send(row);
  });
     
});

app.post('/Ambrosia/src/selectMembroEquipe.js', function(req, res) {
  var row = [];

  var sql = "SELECT b.mem_id, b.mem_nome FROM tb_membro_equipe a join tb_membro b on a.mem_id = b.mem_id WHERE  equi_id = '"+req.body.dadosArquivados+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
        row[key] = result[key];
        console.log(row[key])
      });
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.send(row);
  });
     
});

app.post('/alterarImpedimento', function(req, res) {
  var row;

  var dataFimFormatada = req.body.calendario;
  dataFimFormatada = dataFimFormatada.substring(6,10)+'-'+dataFimFormatada.substring(3,5)+'-'+dataFimFormatada.substring(0,2)
  dataFimFormatada = dataFimFormatada + " 00:00:00"
  var sql = "UPDATE tb_impedimento set imp_msg = '"+req.body.descricao+"', imp_status = '"+req.body.status+"', imp_data = '"+dataFimFormatada+"' WHERE imp_id = '"+req.body.numero+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.redirect('/index');
  });
     
});


app.post('/Ambrosia/src/alterar.js', function(req, res) {

  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;
  var dataFimFormatada = req.body.calendario1;
  dataFimFormatada = dataFimFormatada.substring(6,10)+'-'+dataFimFormatada.substring(3,5)+'-'+dataFimFormatada.substring(0,2)
  dataFimFormatada = dataFimFormatada + " 00:00:00"
  var dataIniFormatada = req.body.calendario;
  dataIniFormatada = dataIniFormatada.substring(6,10)+'-'+dataIniFormatada.substring(3,5)+'-'+dataIniFormatada.substring(0,2)
  dataIniFormatada = dataIniFormatada + " 00:00:00"
  var dataFimRealFormatada = req.body.calendario3;
  dataFimRealFormatada = dataFimRealFormatada.substring(6,10)+'-'+dataFimRealFormatada.substring(3,5)+'-'+dataFimRealFormatada.substring(0,2)
  dataFimRealFormatada = dataFimRealFormatada + " 00:00:00"
  var dataIniRealFormatada = req.body.calendario2;
  dataIniRealFormatada = dataIniRealFormatada.substring(6,10)+'-'+dataIniRealFormatada.substring(3,5)+'-'+dataIniRealFormatada.substring(0,2)
  dataIniRealFormatada = dataIniRealFormatada + " 00:00:00"
  console.log(req.body.sqlExecute);
  console.log()

  var sql = "UPDATE tb_tarefa set tare_titulo = '"+req.body.titulo+"', tare_descri = '"+req.body.descricao+"', tare_dtIni = '"+dataIniFormatada+"', tare_dtEnc = '"+dataFimFormatada+"', tare_status = '"+req.body.status+"', tare_timestamp = '"+dateTime+"', tare_dtRealIni = '"+dataIniRealFormatada+"', tare_dtRealEnc = '"+dataFimRealFormatada+"' WHERE tare_id = '"+req.body.numero+"'";
  var sql3 = "DELETE FROM tb_membro_tarefa WHERE  tare_id = '"+req.body.numero+"'";
  var sql2 = "INSERT INTO tb_membro_tarefa (mem_id, tare_id) values ?";
  banco.connection.query(sql3, function (err, result) {
    if (err) throw err;
    console.log("tabela de ligação LIMPA");
  });
  banco.connection.query(sql2, [req.body.sqlExecute], function (err, result) {
    if (err) throw err;
    console.log("tabela de ligação atualizada");
  });
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record updated");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
  
    res.redirect('/index');
  });
});

app.post('/Ambrosia/src/alterarMembro.js', function(req, res) {

  var senhaParaSalvar = bcrypt.hashSync(req.body.senha, salt);
  var sql = "UPDATE tb_membro set mem_nome = '"+req.body.nome+"', mem_cpf = '"+req.body.cpf+"', mem_senha = '"+senhaParaSalvar+"' WHERE mem_id = '"+req.body.numero+"'";

  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Membro atualizado");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
  
    res.redirect('/index');
  });
});

app.post('/Ambrosia/src/alterarGerente.js', function(req, res) {
  var senhaParaSalvar = bcrypt.hashSync(req.body.senha, salt);

  var sql = "UPDATE tb_gerente set gere_nome = '"+req.body.nome+"', gere_cpf = '"+req.body.cpf+"', gere_senha = '"+senhaParaSalvar+"' WHERE gere_id = '"+req.body.numero+"'";

  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Membro atualizado");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
  
    res.redirect('/index');
  });
});

app.post('/Ambrosia/src/alterarEquipe.js', function(req, res) {

  var sql = "UPDATE tb_equipe set equi_nome = '"+req.body.nome+"' WHERE equi_id = '"+req.body.numero+"'"
  var sql3 = "DELETE FROM tb_membro_equipe WHERE  equi_id = '"+req.body.numero+"'";
  var sql2 = "INSERT INTO tb_membro_equipe (mem_id, equi_id) values ?";
  banco.connection.query(sql3, function (err, result) {
    if (err) throw err;
    console.log("tabela de ligação LIMPA");
  });
  banco.connection.query(sql2, [req.body.sqlExecute], function (err, result) {
    if (err) throw err;
    console.log("tabela de ligação atualizada");
  });
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Equipe alterada");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
  
    res.redirect('/index');
  });

});

app.post('/Ambrosia/src/inserir.js', function(req, res) {

  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;

  var dataFimFormatada = req.body.calendario2;
  dataFimFormatada = dataFimFormatada.substring(6,10)+'-'+dataFimFormatada.substring(3,5)+'-'+dataFimFormatada.substring(0,2)
  dataFimFormatada = dataFimFormatada + " 00:00:00" 

  var dataIniFormatada = req.body.calendario;
  dataIniFormatada = dataIniFormatada.substring(6,10)+'-'+dataIniFormatada.substring(3,5)+'-'+dataIniFormatada.substring(0,2)
  dataIniFormatada = dataIniFormatada + " 00:00:00" 

  var sql = "INSERT INTO tb_tarefa (tare_titulo, tare_descri, tare_dtIni, tare_dtEnc, tare_status, tare_timestamp, tare_gerente) VALUES ('"+req.body.titulo+"', '"+req.body.descricao+"', '"+dataIniFormatada+"', '"+dataFimFormatada+"', '"+req.body.status+"', '"+dateTime+"', '"+req.body.usuario+"')";

  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    res.redirect('/index.html');
  });

});

app.post('/Ambrosia/src/inserirMembro.js', function(req, res) {

 
  var senhaParaSalvar = bcrypt.hashSync(req.body.senha, salt);

  var sql = "INSERT INTO tb_membro (mem_nome, mem_cpf, mem_senha) VALUES ('"+req.body.nome+"', '"+req.body.cpf+"', '"+senhaParaSalvar+"')";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Membro inserido com sucesso");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    res.redirect('/index');
  });
});

app.post('/Ambrosia/src/inserirGerente.js', function(req, res) {


  var senhaParaSalvar = bcrypt.hashSync(req.body.senha, salt);

  var sql = "INSERT INTO tb_gerente (gere_nome, gere_cpf, gere_senha) VALUES ('"+req.body.nome+"', '"+req.body.cpf+"', '"+senhaParaSalvar+"')";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Membro inserido com sucesso");
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    res.redirect('/index');
  });
});

app.post('/Ambrosia/src/inserirEquipe.js', function(req, res) {

  var today = new Date();
  var id;
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;

  console.log(req.body);
  var sql = "INSERT INTO tb_equipe (equi_nome, equi_data_criada, equi_gerente) VALUES ('"+req.body.nome+"', '"+dateTime+"', '"+req.body.usuario+"')";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Equipe inserida com sucesso");
  });

  var sql3 = "select equi_id from tb_equipe WHERE equi_nome = '"+req.body.nome+"'";
  banco.connection.query(sql3, function (err, result) {
    if (err) throw err;
    console.log(result[0].equi_id)
    req.body.sqlExecute.forEach(myFunction)
    function myFunction(item) {
      var membro = item[0]
      var sql2 = "INSERT INTO tb_membro_equipe (equi_id, mem_id) values ('"+result[0].equi_id+"','"+membro+"')";
      banco.connection.query(sql2, function (err, result) {
        if (err) throw err;
        console.log("Membros associado com sucesso na equipe");
      });  
    }
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.redirect('/index');
                                   
  });
  
  
     
  
});

app.post('/Ambrosia/src/inserirImpedimento.js', function(req, res) {

  var dataIniFormatada = req.body.calendario;
  dataIniFormatada = dataIniFormatada.substring(6,10)+'-'+dataIniFormatada.substring(3,5)+'-'+dataIniFormatada.substring(0,2)
  dataIniFormatada = dataIniFormatada + " 00:00:00"

  var sql = "INSERT INTO tb_impedimento (imp_tarefa, imp_data, imp_msg, imp_status) VALUES ('"+req.body.numero+"', '"+dataIniFormatada+"', '"+req.body.descricao+"', '"+req.body.status+"')";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
      res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
      res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
      res.redirect('/index');
  });
});

              app.get("/data/:id", function(req, res){
                console.log(req.params.id)
                var row = [{
                  text: 't',
                  start_date: 'd',
                  end_date: 'd',
                  data_inicio : 'd',
                  data_fim : 'd',
                  id: 0,
                  open: true,
                  nomeMembro: 'a',
                  nomeEquipe: 'e',
                }];
                banco.connection.query("select t.tare_id, t.tare_dtIni, t.tare_dtEnc, t.tare_titulo, mt.tare_id, em.mem_id,em.equi_id, e.equi_nome" + 
                " from tb_tarefa as t JOIN tb_membro_tarefa as mt ON t.tare_id = mt.tare_id JOIN tb_membro_equipe as em ON mt.mem_id = em.mem_id JOIN tb_equipe as e ON e.equi_id = em.equi_id" +
                " where t.tare_gerente = "+req.params.id+" ORDER BY em.equi_id;", 
                function(err, rows){
                    if (err) console.log(err);
                    banco.connection.query("SELECT mem_nome, tare_id FROM tb_membro AS m INNER JOIN tb_membro_tarefa AS mt ON m.mem_id = mt.mem_id", 
                    function(err, mem){
                        if (err) console.log(err);
                        for (var i = 0; i < rows.length; i++){
                            row[i] = Object.create(row);
                            row[i].id = rows[i].tare_id;
                            row[i].start_date = moment(rows[i].tare_dtIni).format('DD/MM/YYYY')
                            //row[i].end_date = rows[i].tare_dtEnc.format("DD/MM/YYYY");
                            row[i].end_date = moment(rows[i].tare_dtEnc).format('DD/MM/YYYY')
                            //row[i].data_inicio = rows[i].tare_dtIni.format("DD/MM/YYYY");
                            row[i].data_inicio = moment(rows[i].tare_dtIni).format('DD/MM/YYYY')
                            //row[i].data_fim = rows[i].tare_dtEnc.format("DD/MM/YYYY");
                            row[i].data_fim = moment(rows[i].tare_dtEnc).format('DD/MM/YYYY')
                            row[i].text = rows[i].tare_titulo;
                            row[i].nomeEquipe = rows[i].equi_nome;
                            row[i].nomeMembro = "a";
                            for (var j = 0; j < mem.length; j++){
                                if (row[i].id == mem[j].tare_id){
                                  if(row[i].nomeMembro == "a"){
                                    row[i].nomeMembro = mem[j].mem_nome;
                                  }else{
                                    row[i].nomeMembro = row[i].nomeMembro + ", "+ mem[j].mem_nome;
                                  }
                                }
                                    
                            }
                            console.log(row[i].start_date);
                            row[i].open = true;
                        }
                        //console.log(rows);
                        //res.send({ data:rows });
                        res.send({ data:row , rows:rows});
                    });
                });
            });

            app.get("/dataMembro/:id", function(req, res){
              console.log(req.params.id)
              var row = [{
                  text: 't',
                  start_date: 'd',
                  data_inicio : 'd',
                  data_fim : 'd',
                  id: 0,
                  open: true,
                  end_date: 'd'
              }];
              banco.connection.query("select  t.tare_id, t.tare_dtIni, t.tare_dtEnc, t.tare_titulo from tb_tarefa t join tb_membro_tarefa m on m.tare_id = t.tare_id where m.mem_id = '"+req.params.id+"' ", 
              function(err, rows){
                  if (err) console.log(err);
                  console.log(rows)
                      for (var i = 0; i < rows.length; i++){
                         
                          row[i] = Object.create(row);
                          row[i].id = rows[i].tare_id;
                          row[i].start_date = moment(rows[i].tare_dtIni).format('DD/MM/YYYY')
                          row[i].end_date = moment(rows[i].tare_dtEnc).format('DD/MM/YYYY')
                          row[i].data_inicio = moment(rows[i].tare_dtIni).format('DD/MM/YYYY')
                          row[i].data_fim = moment(rows[i].tare_dtEnc).format('DD/MM/YYYY')
                          row[i].text = rows[i].tare_titulo;
                          row[i].open = true;
                      }
                      //console.log(rows);
                      //res.send({ data:rows });
                      res.send({ data:row , rows:rows});
              });
          });

app.get("/Ambrosia/src/buscarDesenvolvedor.js", function(req, res){

  var row = [];

  var sql = "SELECT mem_nome, mem_id FROM tb_membro";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
      //console.log(result[key])
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');

    res.send(row);
       
  });
});

app.get("/Ambrosia/src/buscarGerente.js", function(req, res){

  var row = [];

  var sql = "SELECT gere_nome, gere_id FROM tb_gerente";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');

    res.send(row);
       
  });
});

app.get("/Ambrosia/src/buscarEquipes.js", function(req, res){

  var row = [];

  var sql = "SELECT equi_nome, equi_id FROM tb_equipe";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
      //console.log(result[key])
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');

    res.send(row);
       
  });
});

app.post("/listarTarefas", function(req, res){

  var row = [];
  console.log(req.body.usuario)
  var sql = "SELECT * FROM tb_tarefa WHERE tare_gerente = '"+req.body.usuario+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
      //console.log(result[key])
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});

app.post("/Ambrosia/src/listarTarefasMembro.js", function(req, res){

  var row = [];
  var sql = "SELECT * FROM tb_tarefa a join tb_membro_tarefa b on a.tare_id = b.tare_id  where b.mem_id= '"+req.body.membro+"' ORDER BY a.tare_dtEnc";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});


app.post("/Ambrosia/src/listarEquipes.js", function(req, res){

  var row = [];

  var sql = "SELECT * FROM tb_equipe WHERE equi_gerente = '"+req.body.usuario+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
      //console.log(result[key])
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});

app.get("/Ambrosia/src/listarMembros.js", function(req, res){

  var row = [];

  var sql = "SELECT a.mem_nome, a.mem_cpf FROM tb_membro a ";
  //var sql = "SELECT a.mem_nome, a.mem_cpf, b.equi_nome FROM tb_membro a join tb_equipe b where a.mem_equipe = b.equi_id";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});

app.get("/Ambrosia/src/listarGerentes.js", function(req, res){

  var row = [];

  var sql = "SELECT gere_nome, gere_cpf FROM tb_gerente ";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});

app.post("/listarImpedimento", function(req, res){

  var row = [];

  var sql = "SELECT i.imp_id, i.imp_msg, i.imp_status, i.imp_data, t.tare_titulo FROM tb_impedimento i join tb_tarefa t on t.tare_id = i.imp_tarefa  WHERE tare_gerente = '"+req.body.usuario+"' ORDER BY i.imp_data ASC";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});

app.post("/excluirTarefa", function(req, res){

  var sql = "DELETE FROM tb_tarefa WHERE tare_id = '"+req.body.numero+"'";
  var sql2 = "DELETE FROM tb_membro_tarefa WHERE tare_id = '"+req.body.numero+"'";
  banco.connection.query(sql2, function (err, result) {
    if (err) throw err;
    console.log('exclusão tabela de ligação')
    banco.connection.query(sql, function (err, result) {
      if (err) throw err;
      console.log('exclusão tarefa')
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.redirect('/index');
       
  });
});

app.post("/excluirEquipe", function(req, res){

  var sql = "DELETE FROM tb_equipe WHERE equi_id = '"+req.body.numero+"'";
  var sql2 = "DELETE FROM tb_membro_equipe WHERE equi_id = '"+req.body.numero+"'";
  banco.connection.query(sql2, function (err, result) {
    if (err) throw err;
    console.log('exclusão tabela de ligação')
    banco.connection.query(sql, function (err, result) {
      if (err) throw err;
      console.log('exclusão equipe')
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.redirect('/index');
       
  });
});

app.post("/excluirGerente", function(req, res){

  var sql = "DELETE FROM tb_gerente WHERE gere_id = '"+req.body.numero+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log('exclusão gerente')
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.redirect('/index');
       
  });
});

app.post("/excluirMembro", function(req, res){

  var sql = "DELETE FROM tb_membro WHERE mem_id = '"+req.body.numero+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log('exclusão gerente')
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.redirect('/index');
       
  });
});

app.post("/excluirImpedimento", function(req, res){

  var sql = "DELETE FROM tb_impedimento WHERE imp_id = '"+req.body.numero+"'";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log('exclusão gerente')
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.redirect('/index');
       
  });
});

app.post("/Ambrosia/src/graficoTarefa.js", function(req, res){

  var row = [];
  res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
  console.log(req.body);

  var sql = "SELECT count(tare_id), tare_status FROM tb_tarefa WHERE tare_gerente = '"+req.body.usuario+"' group by tare_status;";
  banco.connection.query(sql, function (err, result) {
    if (err) throw err;
    Object.keys(result).forEach(function(key) {
      row[key] = result[key];
      console.log(result[key])
    });
    res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,HEAD,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'content-Type,x-requested-with');
    
    res.send(row);
       
  });
});


app.get('/index', function(req, res) {
  console.log("reached root!");
  res.sendFile(__dirname + '/views/index.html');
})


app.listen(3000, function() {
  console.log('Server running at 3000');
});